<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 23/03/17
 * Time: 14:41
 */

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class AbstractDatabaseRepository implements IDatabaseRepository
{
    /**
     * @var
     */
    protected $model;
    /**
     * @var App
     */
    private $app;

    /**
     * @param App $app
     * @throws \Exception
     */

    public function __construct() {
        $this->app = App::getInstance();
        $this->makeModel();
    }

    /**
     * @return Model
     * @throws \Exception
     */
    public function makeModel() {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*'))
    {
        return $this->model->get($columns);
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*'))
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute="id")
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    /**
     * Delete records by column value
     * @param $attribute
     * @param $value
     * @return mixed
     */
    public function deleteBy($attribute, $value)
    {
        return $this->model->where($attribute, '=', $value)->delete();
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        return $this->model->find($id, $columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findAllBy($attribute, $value, $columns = array('*'))
    {
        return $this->model->where($attribute, '=', $value)->get($columns);
    }


    /**
     * @param array $fieldValues
     * @param array $columns
     * @return mixed
     */
    public function findByMultipleWhere(array $fieldValues, $columns = array('*'))
    {
        return $this->model->where($fieldValues)->first($columns);
    }

    /**
     * @param array $fieldValues
     * @param array $columns
     * @return mixed
     */
    public function findByMultipleWhereAll(array $fieldValues, $columns = array('*'))
    {
        return $this->model->where($fieldValues)->get($columns);
    }
}