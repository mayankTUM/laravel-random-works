<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 23/03/17
 * Time: 14:41
 */

namespace App\Repositories;


interface IDatabaseRepository
{
    public function all($columns = array('*'));

    public function paginate($perPage = 15, $columns = array('*'));

    public function create(array $data);

    public function update(array $data, $id);

    public function delete($id);

    public function find($id, $columns = array('*'));

    public function findBy($field, $value, $columns = array('*'));

    public function findByMultipleWhere(array $fieldValues, $columns = array('*'));

    public function findByMultipleWhereAll(array $fieldValues, $columns = array('*'));
}