<?php

namespace App\Repositories;
use Illuminate\Foundation\Auth\User;

/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 23/03/17
 * Time: 14:35
 */
class UserRepository extends AbstractDatabaseRepository
{
    public function model()
    {
        return User::class;
    }
}