<?php

namespace App\Http\Exceptions;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 23/03/17
 * Time: 14:25
 */
class InvalidCredentialsException extends UnauthorizedHttpException
{
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct('', $message, $previous, $code);
    }
}