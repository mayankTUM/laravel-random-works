/**
 * Created by chaudhary on 23/03/17.
 */
main.controller('LoginController', function ($scope, $http, $window) {
    $scope.attemptLogin = function () {
        console.log("attempting login ...");

        $http({
            method: 'POST',
            url: '/api/login',
            data: {
                email: $scope.email,
                password: $scope.password,
            }
        }).then(function successCallback(response) {
            console.log(response);
            $window.location.href = "/home";

        }, function errorCallback(response) {
            console.log(response);
        });
    };
});