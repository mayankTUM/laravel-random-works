<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('/login', 'App\Http\Controllers\API\LoginREST@login');
    $api->post('/login/refresh', 'App\Http\Controllers\API\LoginREST@refresh');

    $api->group(['middleware' => 'auth:api'], function($api){
        $api->post('/logout', 'App\Http\Controllers\API\LoginREST@logout');
    });
});


