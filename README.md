# README #

This directory includes implementations of new features that are available in Laravel 5.3. The list will be continously updated.

### What is this repository for? ###

* This repository will show you the implementations of several new and important features that are made available in Laravel 5.3 recently.
* Version 1.0

### The development stack is composed of ###

* Laravel 5.3
* MySql
* Vagrant, Homestead
* Angular 1.6

### The projects implemented so far ###

* **Passport**: Shows you how to make your own OAuth 2.0 server using Laravel as the back-end platform.
* **Satellizer**: Contains an implementation of AngularJS authentication module, named Satellier, which allows you to manage all authentication activities such as Log In and Sign Up at Angular end. It uses JWT (JSON Web Tokens) to establish token based authentication, which in turn is implemented at Laravel end.

### Who do I talk to? ###

* Mayank Chaudhary
* mailto:mayankc3266@gmail.com
* skype:mayank.tum