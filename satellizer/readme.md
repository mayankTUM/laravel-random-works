## AngularJS-Satellizer-Laravel Authentication

The work done in this directory shows you how one can easily add token-based
authentication mechanism to your project. A token based authentication module
for AngularJS named, **Satellizer** is used at the front-end to manage all
the authentication related activities. On the back-end side i.e. **Laravel**
end, a package named, **JWT** (short for JSON Web Token) has been used for
generating the authentication tokens. It is also possible to migrate to
Laravel Passport (instead of JWTs) very easily.

In addition to understanding how Laravel and AngularJS works, a slight amount
of knowledge of below mentioned resources will be quite helpful

- [JWT](https://github.com/tymondesigns/jwt-auth).
- [Satellizer](https://github.com/sahat/satellizer).
- [Passport](https://laravel.com/docs/5.3/passport).