<!doctype html>
<html lang=”en” ng-app="authApp">
<head>
    <title>Simple REST API authentication service with Laravel, AngularJS, JWT and Satellizer</title>
    <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
</head>
<body>
    <div ui-view></div>

    <script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('/bower_components/angular/angular.js') }}"></script>
    <script src="{{ asset('/bower_components/angular-ui-router/release/angular-ui-router.min.js') }}"></script>
    <script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/bower_components/satellizer/dist/satellizer.min.js') }}"></script>
    <script src="{{ asset('/scripts/app.js') }}"></script>
</body>
</html>
