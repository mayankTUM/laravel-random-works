/**
 * Created by chaudhary on 31/03/17.
 */
angular.module("authApp", ['ui.router', 'satellizer'])
    .config(function ($stateProvider, $urlRouterProvider, $authProvider) {
       $stateProvider
           .state('login', {
                url: '/login',
                templateUrl: 'templates/login.tpl.html',
                controller: 'LoginCtrl as login',
                authenticate: false,
            })
           .state('register', {
               url: '/register',
               templateUrl: 'templates/register.tpl.html',
               controller: 'RegisterCtrl as register',
               authenticate: false,
           })
           .state('dashboard', {
               url: '/dashboard',
               templateUrl: 'templates/dashboard.tpl.html',
               controller: 'DashboardCtrl as dashboard',
               authenticate: true,
           });

        $urlRouterProvider.otherwise('/login');


        $authProvider.loginUrl = 'http://intense-falls-69712.herokuapp.com/api/login';
        $authProvider.signupUrl = 'http://intense-falls-69712.herokuapp.com/api/register';

        $authProvider.facebook({
            clientId: 'YOUR FACEBOOK CLIENT ID',
            name: 'facebook',
            url: '/api/auth/facebook',
            authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
            redirectUri: window.location.origin + '/',
            requiredUrlParams: ['display', 'scope'],
            scope: ['email', 'public_profile'],
            scopeDelimiter: ',',
            display: 'popup',
            oauthType: '2.0',
            popupOptions: { width: 580, height: 400 }
        });

        $authProvider.linkedin({
            clientId: 'YOUR LINKEDIN CLIENT ID',
            url: '/api/auth/linkedin',
            authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
            redirectUri: window.location.origin,
            requiredUrlParams: ['state'],
            scope: ['r_emailaddress', 'r_basicprofile'],
            scopeDelimiter: ' ',
            state: 'STATE',
            oauthType: '2.0',
            popupOptions: { width: 527, height: 582 }
        });

        $authProvider.google({
            clientId: 'YOUR GOOGLE CLIENT ID',
            url: '/api/auth/google',
            authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
            redirectUri: window.location.origin,
            requiredUrlParams: ['scope'],
            optionalUrlParams: ['display'],
            scope: ['profile', 'email'],
            scopePrefix: 'openid',
            scopeDelimiter: ' ',
            display: 'popup',
            oauthType: '2.0',
            popupOptions: { width: 452, height: 633 }
        });
    }).controller('LoginCtrl', function ($state, $auth) {
        var vm = this;

        vm.user = {};
        vm.login = function __login() {
            $auth.login({
                email: vm.user.email,
                password: vm.user.password
            }).then(function (response) {
                console.log(response);
                $state.go('dashboard');
            }).catch(function (response) {
                console.log(response);
                window.alert('Error: Login failed');
            });
        };

        vm.authenticate = function (provider) {
            $auth.authenticate(provider)
                .then(function (response) {
                    console.log(response);
                    $state.go('dashboard');
                })
                .catch(function (response) {
                    console.log(response);
                    window.alert('Error: Login failed');
                });
            console.log("authentication operation performed");
        }
    }).controller('RegisterCtrl', function ($state, $auth) {
        var vm = this;
        vm.user = {};

        vm.register = function __register() {
            $auth.signup({
                name: vm.user.name,
                email: vm.user.email,
                password: vm.user.password
            }).then(function (response) {
                console.log(response);
                $auth.setToken(response);
                $state.go('dashboard');
            }).catch(function (response) {
                console.log(response);
                window.alert('Error: Register failed');
            });
        };
    }).controller('DashboardCtrl', function ($state, $auth) {
        var vm = this;
        vm.logout = function __logout() {
            $auth.logout();
            $state.go('login');
        }
    }).run(function ($rootScope, $state, $auth) {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            if(toState.authenticate && !$auth.isAuthenticated()) {
                $state.transitionTo("login");
                event.preventDefault();
            }

            if(toState.url == "/login" && $auth.isAuthenticated()) {
                $state.transitionTo("dashboard");
                event.preventDefault();
            }
        })
    });


