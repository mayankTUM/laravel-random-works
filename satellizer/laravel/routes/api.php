<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'v1'], function () {
    Route::post('/login', 'API\AuthREST@login');
    Route::post('/register', 'API\AuthREST@register');
});

Route::group(['prefix' => 'auth'], function() {
    Route::post('/facebook', 'API\FacebookREST@login');
    Route::post('/linkedin', 'API\LinkedInREST@login');
    Route::post('/google', 'API\GoogleREST@login');
});

