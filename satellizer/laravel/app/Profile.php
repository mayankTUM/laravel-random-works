<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'phone_number', 'dob', 'age', 'gender', 'profile_photo',
    ];
}
