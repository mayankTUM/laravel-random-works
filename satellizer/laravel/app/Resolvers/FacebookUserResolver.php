<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 19/04/17
 * Time: 22:39
 */

namespace App\Resolvers;


use App\Repositories\Implementation\ProfileRepository;
use App\Repositories\Implementation\UserRepository;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FacebookUserResolver
{
    private $client;

    private $userRepository;

    private $profileRepository;

    private static $GRAPH_API_URL = "https://graph.facebook.com/v2.8/me";

    private static $PICTURE_URL = "https://graph.facebook.com/v2.8/me/picture";

    /**
     * FacebookUserResolver constructor.
     * @param UserRepository $userRepository
     * @param ProfileRepository $profileRepository
     */
    public function __construct(UserRepository $userRepository, ProfileRepository $profileRepository)
    {
        $this->client = new Client();
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param $accessToken
     * @return mixed|null
     * @throws \Exception
     */
    public function authWithFacebook($accessToken) {
        /* get the basic details from the social account */
        $params = [
            'query' => [
                'access_token' => $accessToken,
                'fields' => 'id, email,first_name,last_name,gender,verified,age_range',
            ]
        ];

        $res = $this->client->get(self::$GRAPH_API_URL, $params);
        if(!($res->getStatusCode() == 200)) {
            return response()->json(['cannot login'], 401);
        }
        $body = json_decode($res->getBody(), true);

        /* get the profile picture from the social account */
        $params = [
            'query' => [
                'access_token' => $accessToken,
                'type' => 'album',
                'redirect' => 0,
            ]
        ];

        $res = $this->client->get(self::$PICTURE_URL, $params);
        if(!($res->getStatusCode() == 200)) {
            return response()->json(['cannot login'], 401);
        }

        $pic = json_decode($res->getBody(), true);

        /* creates the unified data array */
        $data = [
            'email' => $body['email'],
            'facebook_id' => $body['id'],
            'first_name' => $body['first_name'],
            'last_name' => $body['last_name'],
            'gender' => $body['gender'],
            'verified' => $body['verified'],
            'profile_photo' => $pic['data']['url'],
        ];

        if(!$body['verified']) {
            throw new \Exception("Your Facebook account itself is not verified. We cannot register you with Facebook. Try other registration options");
        }

        $user = $this->createIfDoesNotExist($data);
        return $user;
    }

    /**
     * @param $data
     * @return mixed|null
     */
    private function createIfDoesNotExist($data) {
        if(!$this->userAlreadyExists($data)) {
            $user = $this->createUser($data);
            return $user;
        }
        $token = $this->getUser($data);
        return $token;
    }

    /**
     * @param $data
     * @return bool
     */
    private function userAlreadyExists($data) {
        $rules = array('email' => 'unique:users,email');
        $validator = Validator::make($data, $rules);
        if($validator->fails()) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return null
     */
    private function createUser($data) {
        DB::beginTransaction();
        try {
            event(new Registered($user = $this->create($data)));
            if(!is_null($user)) {
                DB::commit();
                return $user;
            }
            return null;
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    private function create($data) {
        $user = $this->userRepository->create([
            'email' => $data['email'],
            'facebook_id' => $data['facebook_id'],
            'avatar' => $data['profile_photo'],
        ]);

        $profile = $this->profileRepository->create([
            'user_id' => $user->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'profile_photo' => $data['profile_photo'],
            'gender' => ucfirst($data['gender']),
        ]);

        return $user;
    }

    /**
     * @param $data
     * @return mixed
     */
    private function getUser($data) {
        $user = $this->userRepository->findBy('email', $data['email']);
        return $user;
    }
}