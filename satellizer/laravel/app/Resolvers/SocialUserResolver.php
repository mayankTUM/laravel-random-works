<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 18/04/17
 * Time: 13:39
 */

namespace App\Resolvers;


use Adaojunior\Passport\SocialGrantException;
use Adaojunior\Passport\SocialUserResolverInterface;
use App\Repositories\Implementation\UserRepository;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SocialUserResolver implements SocialUserResolverInterface
{
    private $facebookUserResolver;

    private $linkedInUserResolver;

    private $googleUserResolver;

    /**
     * SocialUserResolver constructor.
     * @param FacebookUserResolver $facebookUserResolver
     * @param LinkedInUserResolver $linkedInUserResolver
     * @param GoogleUserResolver $googleUserResolver
     */
    public function __construct(FacebookUserResolver $facebookUserResolver, LinkedInUserResolver $linkedInUserResolver, GoogleUserResolver $googleUserResolver)
    {
        $this->facebookUserResolver = $facebookUserResolver;
        $this->linkedInUserResolver = $linkedInUserResolver;
        $this->googleUserResolver = $googleUserResolver;
    }

    public function resolve($network, $accessToken)
    {
        switch ($network) {
            case 'facebook':
                return $this->facebookUserResolver->authWithFacebook($accessToken);
                break;
            case 'linkedin':
                return $this->linkedInUserResolver->authWithLinkedIn($accessToken);
                break;
            case 'google':
                return $this->googleUserResolver->authWithGoogle($accessToken);
                break;
            default:
                throw SocialGrantException::invalidNetwork();
                break;
        }
    }
}