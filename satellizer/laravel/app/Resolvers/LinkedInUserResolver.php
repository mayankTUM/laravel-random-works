<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 21/04/17
 * Time: 12:52
 */

namespace app\Resolvers;


use App\Repositories\Implementation\ProfileRepository;
use App\Repositories\Implementation\UserRepository;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LinkedInUserResolver
{
    private $client;

    private $userRepository;

    private $profileRepository;

    private static $PROFILE_URL = "https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,picture-url)?format=json";

    /**
     * LinkedInUserResolver constructor.
     * @param UserRepository $userRepository
     * @param ProfileRepository $profileRepository
     */
    public function __construct(UserRepository $userRepository, ProfileRepository $profileRepository)
    {
        $this->client = new Client();
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
    }

    /**
     * @param $accessToken
     * @return mixed|null
     */
    public function authWithLinkedIn($accessToken) {
        $params = [
            'headers' => [
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ];

        $res = $this->client->get(self::$PROFILE_URL, $params);

        if(!($res->getStatusCode() == 200)) {
            return response()->json(['cannot login'], 401);
        }
        $body = json_decode($res->getBody(), true);

        $data = [
            'email' => $body['emailAddress'],
            'first_name' => $body['firstName'],
            'last_name' => $body['lastName'],
            'linkedin_id' => $body['id'],
            'profile_photo' => $body['pictureUrl'],
        ];

        $user = $this->createIfDoesNotExist($data);
        return $user;
    }

    /**
     * @param $data
     * @return mixed|null
     */
    private function createIfDoesNotExist($data) {
        if(!$this->userAlreadyExists($data)) {
            $user = $this->createUser($data);
            return $user;
        }
        $token = $this->getUser($data);
        return $token;
    }

    /**
     * @param $data
     * @return bool
     */
    private function userAlreadyExists($data) {
        $rules = array('email' => 'unique:users,email');
        $validator = Validator::make($data, $rules);
        if($validator->fails()) {
            return true;
        }
        return false;
    }

    /**
     * @param $data
     * @return null
     */
    private function createUser($data) {
        DB::beginTransaction();
        try {
            event(new Registered($user = $this->create($data)));
            if(!is_null($user)) {
                DB::commit();
                return $user;
            }
            return null;
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * @param $data
     * @return mixed
     */
    private function create($data) {
        $user = $this->userRepository->create([
            'email' => $data['email'],
            'linkedin_id' => $data['linkedin_id'],
            'avatar' => $data['profile_photo'],
        ]);

        $profile = $this->profileRepository->create([
            'user_id' => $user->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'profile_photo' => $data['profile_photo'],
        ]);

        return $user;
    }


    /**
     * @param $data
     * @return mixed
     */
    private function getUser($data) {
        $user = $this->userRepository->findBy('email', $data['email']);
        return $user;
    }



}