<?php

/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 17/04/17
 * Time: 14:01
 */

namespace App\Enums;

class Gender
{
    const MALE = "Male";
    const FEMALE = "Female";
    const OTHER = "Other";

    public static function getAllGenders() {
        $gender = array();
        array_push($gender, Gender::MALE);
        array_push($gender, Gender::FEMALE);
        array_push($gender, Gender::OTHER);
        return $gender;
    }
}