<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 19/04/17
 * Time: 23:23
 */

namespace App\Services;


use App\Proxies\LoginProxy;
use GuzzleHttp\Client;

class LinkedInUserService
{
    private static $ACCESS_TOKEN_URL = "https://www.linkedin.com/oauth/v2/accessToken";

    private $client;

    private $loginProxy;

    /**
     * LinkedInUserService constructor.
     */
    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
        $this->client = new Client();
    }

    public function attemptLogin($request) {
        /* get the access token from authorization code */
        $params = [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $request->get('clientId'),
                'client_secret' => env('LINKEDIN_APP_SECRET'),
                'redirect_uri' => $request->get('redirectUri'),
                'code' => $request->get('code'),
            ]
        ];

        $res = $this->client->post(self::$ACCESS_TOKEN_URL, $params);
        if(!($res->getStatusCode() == 200)) {
            return response()->json(['cannot login'], 401);
        }

        $body = json_decode($res->getBody());
        $access_token = $body->access_token;

        /* call the appropriate social user resolver */
        $token_array = $this->loginProxy->proxy('social', [
            'access_token' => $access_token,
            'network' => 'linkedin',
        ]);

        return $token_array;
    }


}