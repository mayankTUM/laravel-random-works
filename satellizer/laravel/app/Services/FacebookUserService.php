<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 03/04/17
 * Time: 17:27
 */

namespace App\Services;


use App\Proxies\LoginProxy;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FacebookUserService
{
    private static $ACCESS_TOKEN_URL = "https://graph.facebook.com/v2.8/oauth/access_token";

    private $client;

    private $loginProxy;

    /**
     * FacebookUserService constructor.
     * @param LoginProxy $loginProxy
     */
    public function __construct(LoginProxy $loginProxy)
    {
        $this->client = new Client();
        $this->loginProxy = $loginProxy;
    }

    public function attemptLogin($request) {
        /* get the access token from authorization code */
        $params = [
            'query' => [
                'client_id' => $request->get('clientId'),
                'client_secret' => env('FACEBOOK_APP_SECRET'),
                'redirect_uri' => $request->get('redirectUri'),
                'code' => $request->get('code'),
            ]
        ];

        $res = $this->client->get(self::$ACCESS_TOKEN_URL, $params);
        if(!($res->getStatusCode() == 200)) {
            return response()->json(['cannot login'], 401);
        }

        $body = json_decode($res->getBody());
        $access_token = $body->access_token;

        /* call the appropriate social user resolver */
        $token_array = $this->loginProxy->proxy('social', [
            'access_token' => $access_token,
            'network' => 'facebook',
        ]);

        return $token_array;
    }

}