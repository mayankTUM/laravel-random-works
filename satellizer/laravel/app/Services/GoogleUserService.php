<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 21/04/17
 * Time: 16:14
 */

namespace App\Services;


use App\Proxies\LoginProxy;
use GuzzleHttp\Client;

class GoogleUserService
{
    private $client;

    private $loginProxy;

    private static $TOKEN_URL = "https://accounts.google.com/o/oauth2/token";

    /**
     * GoogleUserService constructor.
     */
    public function __construct(LoginProxy $loginProxy)
    {
        $this->client = new Client();
        $this->loginProxy = $loginProxy;
    }


    public function attemptLogin($request) {
        $params = [
            'headers' => [
                'Content-Type' => "application/x-www-form-urlencoded"
            ],
            'form_params' => [
                'client_id' => $request->get('clientId'),
                'client_secret' => env('GOOGLE_APP_SECRET'),
                'redirect_uri' => $request->get('redirectUri'),
                'code' => $request->get('code'),
                'grant_type' => 'authorization_code',
            ]
        ];

        $res = $this->client->post(self::$TOKEN_URL, $params);
        if(!($res->getStatusCode() == 200)) {
            return response()->json(['cannot login'], 401);
        }

        $body = json_decode($res->getBody());
        $access_token = $body->access_token;

        /* call the appropriate social user resolver */
        $token_array = $this->loginProxy->proxy('social', [
            'access_token' => $access_token,
            'network' => 'google',
        ]);

        return $token_array;
    }
}