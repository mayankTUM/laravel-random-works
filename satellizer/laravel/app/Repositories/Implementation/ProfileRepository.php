<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 17/04/17
 * Time: 14:10
 */

namespace App\Repositories\Implementation;


use App\Profile;

class ProfileRepository extends AbstractDatabaseRepository
{
    function model()
    {
        return Profile::class;
    }
}