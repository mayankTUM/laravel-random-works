<?php

namespace App\Repositories\Implementation;
use App\User;

/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 31/03/17
 * Time: 20:59
 */
class UserRepository extends AbstractDatabaseRepository
{
    function model()
    {
        return User::class;
    }
}