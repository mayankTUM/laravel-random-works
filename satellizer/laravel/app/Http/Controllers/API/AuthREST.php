<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use App\Services\UserService;

class AuthREST extends Controller
{

    private $userService;

    /**
     * AuthREST constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param LoginRequest $request
     */
    public function login(LoginRequest $request) {
//        $token = $this->userService->attemptLogin($request);
//        if(!is_null($token)) {
//            return response()->json(compact('token'));
//        }
//        return response()->json(['invalid_email_or_password'], 422);
    }

    /**
     * @param RegisterRequest $request
     */
    public function register(RegisterRequest $request) {
//        $token = $this->userService->createUser($request);
//        if(!is_null($token)) {
//            return response()->json(compact('token'));
//        }
//        return response()->json(['failed_to_create_new_user'], 500);
    }
}
