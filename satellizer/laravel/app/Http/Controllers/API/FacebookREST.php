<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\FacebookUserService;
use GuzzleHttp\Client;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Request;

class FacebookREST extends Controller
{

    private $client;
    
    private $facebookUserService;


    /**
     * FacebookREST constructor.
     * @param FacebookUserService $facebookUserService
     */
    public function __construct(FacebookUserService $facebookUserService)
    {
        $this->client = new Client();
        $this->facebookUserService = $facebookUserService;
    }


    public function login(Request $request) {
        $token = $this->facebookUserService->attemptLogin($request);
        return $token;
    }
}
