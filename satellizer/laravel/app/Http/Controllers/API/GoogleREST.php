<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 21/04/17
 * Time: 16:14
 */

namespace App\Http\Controllers\API;


use App\Services\GoogleUserService;
use Symfony\Component\HttpFoundation\Request;

class GoogleREST
{

    private $googleUserService;

    /**
     * GoogleREST constructor.
     * @param GoogleUserService $googleUserService
     */
    public function __construct(GoogleUserService $googleUserService)
    {
        $this->googleUserService = $googleUserService;
    }

    /**
     * @param Request $request
     */
    public function login(Request $request) {
        $token = $this->googleUserService->attemptLogin($request);
        return $token;
    }
}