<?php
/**
 * Created by PhpStorm.
 * User: chaudhary
 * Date: 21/04/17
 * Time: 12:42
 */

namespace App\Http\Controllers\API;


use App\Services\LinkedInUserService;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;

class LinkedInREST
{
    private $client;

    private $linkedInUserService;

    /**
     * LinkedInREST constructor.
     * @param LinkedInUserService $linkedInUserService
     */
    public function __construct(LinkedInUserService $linkedInUserService)
    {
        $this->client = new Client();
        $this->linkedInUserService = $linkedInUserService;
    }

    public function login(Request $request) {
        $token = $this->linkedInUserService->attemptLogin($request);
        return $token;
    }


}